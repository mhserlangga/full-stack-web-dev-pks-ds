// Soal 1
const myFunction = (luas, kelilling) => {
    console.log ("Sebuah persegi panjang luasnya " + luas + " cm, dengan kelilingnya " + kelilling + " cm");
}
myFunction(20, 15)


// Soal 2
const newFunction = () => {
    const firstName = "William";
    const lastName = "Imoh";
    const fullName = `${firstName} ${lastName}`
    console.log(fullName)
}
newFunction()


// Soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }
const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)


// Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined)


// Soal 5
const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(before)
