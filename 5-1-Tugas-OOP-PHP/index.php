<?php
// Parent class
abstract class Hewan 
{
    public $nama;
    public $darah;
    public $jumlahKaki;
    public $keahlian;

    public function __construct($nama, $keahlian){
        $this->nama = $nama;
        $this->keahlian = $keahlian;
    }

    public function setNama($nama) {
      $this->nama = $nama;
    }

    public function setDarah($darah) {
        $this->darah = $darah;
    }

    public function setJumlahKaki($jumlahKaki) {
        $this->jumlahKaki = $jumlahKaki;
    }

    public function setKeahlian($keahlian) {
        $this->keahlian = $keahlian;
    }

    public function getNama(){
        echo "Nama Hewan : " . $this->nama;
    }

    public function getDarah(){
        echo "Darah : " . $this->darah;
    }

    public function getJumlahKaki(){
        echo "Jumlah Kaki : " . $this->jumlahKaki;
    }

    public function getKeahlian(){
        echo "Keahlian : " . $this->keahlian;
    }

    abstract public function atraksi();
}
  
abstract class Fight extends Hewan
{
    public $attackPower;
    public $defensePower;

    abstract public function serang();
    abstract public function diserang();
}

// Child classes
class Harimau extends Fight {
    public function atraksi() {
        echo "Ini adalah $this->nama yang sedang $this->keahlian";
    }

    public const APH = 7;
    public const DPH = 8;

    public function serang(){
        echo "Harimau sedang menyerang Elang";
    }

    public function diserang(){
        echo "Harimau sedang diserang Elang";
    }

    public function darah(){
        echo self::$this->darah - APH / DPE;
    }


}

class Elang extends Fight {
    public function atraksi() {
        echo "Ini adalah $this->nama yang sedang $this->keahlian";
    }

    public const APE = 10;
    public const DPE = 5;

    public function serang(){
        echo "Elang sedang menyerang Harimau";
    }

    public function diserang(){
        echo "Elang sedang diserang Harimau";
    }
  }

// Create objects from the child classes
$harimau = new Harimau("harimau", "lari cepat");
$harimau->atraksi();
echo "<br>";

$harimau->setNama("Harimau");
$harimau->getNama();
echo "<br>";

$harimau->setJumlahKaki(4);
$harimau->getJumlahKaki();
echo "<br>";

$harimau->setKeahlian("Lari Cepat");
$harimau->getKeahlian();
echo "<br>";

$harimau->setDarah(50);
$harimau->getDarah();
echo "<br>";

echo "Attack Power : " . Harimau::APH;
echo "<br>";

echo "Defense Power : " . Harimau::DPH;
echo "<br>";

$harimau1 = new Harimau(7, 8);
$harimau1->serang();
echo "<br>";

$harimau1->diserang();
echo "<br><br>";


$elang = new Elang("elang", "terbang tinggi");
$elang->atraksi();
echo "<br>";

$elang->setNama("Elang");
$elang->getNama();
echo "<br>";

$elang->setJumlahKaki(2);
$elang->getJumlahKaki();
echo "<br>";

$elang->setKeahlian("Terbang Tinggi");
$elang->getKeahlian();
echo "<br>";

$elang->setDarah(50);
$elang->getDarah();
echo "<br>";

echo "Attack Power : " . Elang::APE;
echo "<br>";

echo "Defense Power : " . Elang::DPE;
echo "<br>";

$elang1 = new Elang(10, 5);
$elang1->serang();
echo "<br>";

$elang1->diserang();
?>