<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Post
Route::get('/post', 'PostController@index');
Route::post('/post', 'PostController@store');
Route::get('/post/{id}', 'PostController@show');
Route::put('/post/{id}', 'PostController@update');
Route::delete('/post/{id}', 'PostController@destroy');

// Roles
Route::get('/role', 'RolesController@index');
Route::post('/role', 'RolesController@store');
Route::get('/role/{id}', 'RolesController@show');
Route::put('/role/{id}', 'RolesController@update');
Route::delete('/role/{id}', 'RolesController@destroy');

// Comments
Route::get('/comment', 'CommentController@index');
Route::post('/comment', 'CommentController@store');
Route::get('/comment/{id}', 'CommentController@show');
Route::put('/comment/{id}', 'CommentController@update');
Route::delete('/comment/{id}', 'CommentController@destroy');