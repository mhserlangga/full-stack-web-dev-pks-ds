<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'description', 'user_id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating( function ($model){
            if( empty ($model->id) ) {
                $model->id = Str::uuid();
            }
        });
    }

    public function comment()
    {
        return $this->hasMany('App\Comments');
    }

    public function user()
    {
        return $this->belongsTo('App\Users');
    }

}
