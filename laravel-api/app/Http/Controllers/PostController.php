<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar post berhasil',
            'data' => $post
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allrequest = $request->all();

        $validator = Validator::make($allrequest, [
            'title' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $post = Post::create([
            'title' => $request->title,
            'description' => $request->description,
        ]);
    
        if($post){
            return response()->json([
                'success' => true,
                'message' => 'Data post berhasil dibuat',
                'data' => $post,
            ],200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data post gagal dibuat'
        ] ,409);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);

        if($post){
            return response()->json([
                'success' => true,
                'message' => 'Data post berhasil ditampilkan'
            ] ,200);
        }


        return response()->json([
            'success' => false,
            'description' => 'Data dengan id : '. $id .' tidak ditemukan',
        ] ,404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allrequest = $request->all();

        $validator = Validator::make($allrequest, [
            'title' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $post = Post::find($id);

        if($post){
            $post->update([
                'title' => $request->title,
                'description' => $request->description
            ]);

            return response()->json([
                'success' => true,
                'description' => 'Data dengan judul : ' . $post->title . ' berhasil diupdate',
                'data' => $post
            ]);
        }

        return response()->json([
            'success' => false,
            'description' => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ] ,404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        if($post){
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data post berhasil dihapus'
            ] ,200);
        }


        return response()->json([
            'success' => false,
            'description' => 'Data dengan id : '. $id .' tidak ditemukan',
        ] ,404);
    }
}
