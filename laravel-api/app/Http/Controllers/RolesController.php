<?php

namespace App\Http\Controllers;

use App\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Roles::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar role berhasil',
            'data' => $roles
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allrequest = $request->all();

        $validator = Validator::make($allrequest, [
            'name' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $roles = Roles::create([
            'name' => $request->name,
        ]);
    
        if($roles){
            return response()->json([
                'success' => true,
                'message' => 'Data role berhasil dibuat',
                'data' => $roles,
            ],200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data role gagal dibuat'
        ] ,409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $roles = Roles::find($id);

        if($roles){
            return response()->json([
                'success' => true,
                'message' => 'Data role berhasil ditampilkan'
            ] ,200);
        }


        return response()->json([
            'success' => false,
            'description' => 'Data dengan id : '. $id .' tidak ditemukan',
        ] ,404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allrequest = $request->all();

        $validator = Validator::make($allrequest, [
            'name' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $roles = Roles::find($id);

        if($roles){
            $roles->update([
                'name' => $request->name,
            ]);

            return response()->json([
                'success' => true,
                'description' => 'Data dengan judul : ' . $roles->name . ' berhasil diupdate',
                'data' => $roles
            ]);
        }

        return response()->json([
            'success' => false,
            'description' => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ] ,404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $roles = Roles::find($id);

        if($roles){
            $roles->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data post berhasil dihapus'
            ] ,200);
        }


        return response()->json([
            'success' => false,
            'description' => 'Data dengan id : '. $id .' tidak ditemukan',
        ] ,404);
    }
}
