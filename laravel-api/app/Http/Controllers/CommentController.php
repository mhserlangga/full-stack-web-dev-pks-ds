<?php

namespace App\Http\Controllers;

use App\Comments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comments::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar comment berhasil',
            'data' => $comments
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allrequest = $request->all();

        $validator = Validator::make($allrequest, [
            'content' => 'required',
            'posts_id' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $comments = Comments::create([
            'content' => $request->content,
            'posts_id' => $request->post_id,
        ]);
    
        if($comments){
            return response()->json([
                'success' => true,
                'message' => 'Data comment berhasil dibuat',
                'data' => $comments,
            ],200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data comment gagal dibuat'
        ] ,409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comments = Comments::find($id);

        if($comments){
            return response()->json([
                'success' => true,
                'message' => 'Data comment berhasil ditampilkan'
            ] ,200);
        }


        return response()->json([
            'success' => false,
            'description' => 'Data dengan id : '. $id .' tidak ditemukan',
        ] ,404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allrequest = $request->all();

        $validator = Validator::make($allrequest, [
            'content' => 'required',
            'posts_id' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $comments = Comments::find($id);

        if($comments){
            $comments->update([
                'content' => $request->content,
                'posts_id' => $request->posts_id
            ]);

            return response()->json([
                'success' => true,
                'description' => 'Data dengan judul : ' . $comments->content . ' berhasil diupdate',
                'data' => $comments
            ]);
        }

        return response()->json([
            'success' => false,
            'description' => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ] ,404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comments = Comments::find($id);

        if($comments){
            $comments->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data comment berhasil dihapus'
            ] ,200);
        }

        return response()->json([
            'success' => false,
            'description' => 'Data dengan id : '. $id .' tidak ditemukan',
        ] ,404);
    }
}