<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $fillable = ['name'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating( function ($model){
            if( empty ($model->id) ) {
                $model->id = Str::uuid();
            }
        });
    }

    public function users()
    {
        return $this->hasMany('App\Users');
    }
}
