<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $fillable = ['username', 'email', 'name', 'roles_id', 'password', 'email_verified_at'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating( function ($model){
            if( empty ($model->id) ) {
                $model->id = Str::uuid();
            }
        });
    }

    public function roles()
    {
        return $this->belongsTo('App\Roles');
    }

    public function otp_code()
    {
        return $this->hasOne('App\OtpCode');
    }

    public function comments()
    {
        return $this->hasMany('App\Comments');
    }

    public function posts()
    {
        return $this->hasMany('App\Posts');
    }
}
